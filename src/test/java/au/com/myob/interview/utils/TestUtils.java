package au.com.myob.interview.utils;

import au.com.myob.interview.model.AnnualTaxRates;
import au.com.myob.interview.validation.AnnualTaxRatesValidatorTest;
import org.apache.commons.lang3.SerializationUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;

public class TestUtils {

    /**
     * Load default tax rates
     * @return taxRates
     * @throws Exception
     */
    public static AnnualTaxRates loadDefaultTaxRates() throws Exception{
        InputStream inputStream = AnnualTaxRatesValidatorTest.class.getClassLoader().getResourceAsStream("annual-tax-rates.yaml");

        // Parse yaml to AnnualTaxRates instance
        return new Yaml().loadAs(inputStream, AnnualTaxRates.class);
    }

    /**
     * Clone default tax rates
     * @return cloned tax rates
     */
    public static AnnualTaxRates cloneDefaultTaxRates(AnnualTaxRates annualTaxRates) {
        return (AnnualTaxRates) SerializationUtils.clone(annualTaxRates);
    }
}
