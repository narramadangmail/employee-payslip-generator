package au.com.myob.interview;

import au.com.myob.interview.exception.PayslipProcessingException;
import au.com.myob.interview.processor.EmployeePayslipProcessor;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Class to test the application which is the entry point for processing employee payroll generation
 */
@ExtendWith(MockitoExtension.class)
public class AppTest {

    private static App app;
    private static EmployeePayslipProcessor employeePayslipProcessor;

    @BeforeAll
    public static void init() throws PayslipProcessingException{
        // Mock employee processor and ensure to not invoke when payslip generation process is invoked.
        employeePayslipProcessor = mock(EmployeePayslipProcessor.class);
        doNothing().when(employeePayslipProcessor).processEmployeePayslipGeneration(anyString(), anyInt());

        // Create instance of app
        app = new App();

        // set mocked employee payslip processor to app
        ReflectionTestUtils.setField(app, "employeePayslipProcessor", employeePayslipProcessor);
    }

    /**
     * Invoke application entrypoint i.e main method by passing Employee name and Annual Income as input arguments.
     * This should process payslip generation successfully as the default annual taxrates configuration file is loaded
     */
    @Test
    public void test_ProcessEmployeePayslipGeneration() {
        try {
            App.main(new String[] {"Madan Narra", "60000"});
        }
        catch (Exception e) {
            fail("Error occurred while processing employee payslip generation", e);
        }
    }

    /**
     * Test method to validate empty arguments
     */
    @Test
    public void test_InvalidInput_EmptyArgs() {
        assertThrows(PayslipProcessingException.class, () -> {
            String[] args = new String[]{};

            app.processEmployeePayslipGeneration(args);
        });
    }

    /**
     * Test method to validate invalid number of arguments passed to the application
     */
    @Test
    public void test_InvalidInput_InvalidArgsLength() {
        assertThrows(PayslipProcessingException.class, () -> {
            String[] args = new String[]{"one", "two", "three"};

            app.processEmployeePayslipGeneration(args);
        });
    }

    /**
     * Test method to validate invalid employee name passed to the application
     */
    @Test
    public void test_InvalidInput_InvalidEmployeeName() {
        assertThrows(PayslipProcessingException.class, () -> {
            String[] args = new String[]{"", "60000"};

            app.processEmployeePayslipGeneration(args);
        });
    }

    /**
     * Test method to validate invalid annual income passed to the application
     */
    @Test
    public void test_InvalidInput_InvalidAnnualIncome() {
        assertThrows(PayslipProcessingException.class, () -> {
            String[] args = new String[]{"Madan Narra", "INVALID"};

            app.processEmployeePayslipGeneration(args);
        });
    }
}
