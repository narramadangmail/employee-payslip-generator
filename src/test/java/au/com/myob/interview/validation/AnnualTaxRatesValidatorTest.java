package au.com.myob.interview.validation;

import au.com.myob.interview.exception.PayslipProcessingException;
import au.com.myob.interview.model.AnnualTaxRates;
import au.com.myob.interview.model.TaxRate;
import au.com.myob.interview.utils.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class to test Annual tax rates validator class
 */
public class AnnualTaxRatesValidatorTest {

    private static AnnualTaxRatesValidator validator;
    private static AnnualTaxRates defaultAnnualTaxRates;

    @BeforeAll
    public static void init() throws Exception {
        defaultAnnualTaxRates = TestUtils.loadDefaultTaxRates();

        validator = new AnnualTaxRatesValidator();
    }

    @Test
    public void test_TaxRates_DoesNotStartWithZero() {

        // Clone default taxrates
        AnnualTaxRates annualTaxRates = TestUtils.cloneDefaultTaxRates(defaultAnnualTaxRates);

        // Set lower cutoff for first band to 0
        List<TaxRate> taxRates = annualTaxRates.getTaxRates().stream().sorted(Comparator.comparingInt(TaxRate::getBand)).collect(Collectors.toList());
        taxRates.get(0).setLowerCutOff(1);

        assertThrows(PayslipProcessingException.class, () -> {
            validator.validateTaxRatesConfiguration(annualTaxRates);
        });
    }

    @Test
    public void test_TaxRates_ContainsDuplicate_Band() {

        // Clone default taxrates
        AnnualTaxRates annualTaxRates = TestUtils.cloneDefaultTaxRates(defaultAnnualTaxRates);

        // Set duplicate bands
        List<TaxRate> taxRates = annualTaxRates.getTaxRates().stream().sorted(Comparator.comparingInt(TaxRate::getBand)).collect(Collectors.toList());
        taxRates.get(1).setBand(taxRates.get(0).getBand());

        assertThrows(PayslipProcessingException.class, () -> {
            validator.validateTaxRatesConfiguration(annualTaxRates);
        });
    }

    @Test
    public void test_TaxRates_ContainsDuplicate_UpperCutoff() {

        // Clone default upper cutoff
        AnnualTaxRates annualTaxRates = TestUtils.cloneDefaultTaxRates(defaultAnnualTaxRates);

        // Set duplicate bands
        List<TaxRate> taxRates = annualTaxRates.getTaxRates().stream().sorted(Comparator.comparingInt(TaxRate::getBand)).collect(Collectors.toList());
        taxRates.get(1).setUpperCutOff(taxRates.get(0).getUpperCutOff());

        assertThrows(PayslipProcessingException.class, () -> {
            validator.validateTaxRatesConfiguration(annualTaxRates);
        });
    }

    @Test
    public void test_TaxRates_ContainsDuplicate_LowerCutOff() {

        // Clone default taxrates
        AnnualTaxRates annualTaxRates = TestUtils.cloneDefaultTaxRates(defaultAnnualTaxRates);

        // Set duplicate lower cutoff
        List<TaxRate> taxRates = annualTaxRates.getTaxRates().stream().sorted(Comparator.comparingInt(TaxRate::getBand)).collect(Collectors.toList());
        taxRates.get(2).setLowerCutOff(taxRates.get(1).getLowerCutOff());

        assertThrows(PayslipProcessingException.class, () -> {
            validator.validateTaxRatesConfiguration(annualTaxRates);
        });
    }

    @Test
    public void test_TaxRates_Range_NotValid() {

        // Clone default taxrates
        AnnualTaxRates annualTaxRates = TestUtils.cloneDefaultTaxRates(defaultAnnualTaxRates);

        // Decrement one of taxrates lower cutoff by one, so it will be same as upper cutoff of its previous element which should fail the validation
        List<TaxRate> taxRates = annualTaxRates.getTaxRates().stream().sorted(Comparator.comparingInt(TaxRate::getBand)).collect(Collectors.toList());
        taxRates.get(2).setLowerCutOff(taxRates.get(2).getLowerCutOff()-1);

        assertThrows(PayslipProcessingException.class, () -> {
            validator.validateTaxRatesConfiguration(annualTaxRates);
        });
    }

    @Test
    public void test_TaxRates_Range_LowerCutoffGreaterThanUpperCutoff() {

        // Clone default taxrates
        AnnualTaxRates annualTaxRates = TestUtils.cloneDefaultTaxRates(defaultAnnualTaxRates);

        // Set one of taxrates upper cutoff limit less than its lower cutoff limit which should fail the validation
        List<TaxRate> taxRates = annualTaxRates.getTaxRates().stream().sorted(Comparator.comparingInt(TaxRate::getBand)).collect(Collectors.toList());
        taxRates.get(2).setUpperCutOff(taxRates.get(2).getLowerCutOff()-100);

        assertThrows(PayslipProcessingException.class, () -> {
            validator.validateTaxRatesConfiguration(annualTaxRates);
        });
    }

}
