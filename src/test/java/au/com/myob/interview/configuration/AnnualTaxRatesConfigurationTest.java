package au.com.myob.interview.configuration;

import au.com.myob.interview.exception.ErrorCodes;
import au.com.myob.interview.exception.PayslipProcessingException;
import au.com.myob.interview.validation.AnnualTaxRatesValidator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.springframework.test.util.ReflectionTestUtils;

/**
 * Class to test annual tax rates configuration class
 */
public class AnnualTaxRatesConfigurationTest {

    private static AnnualTaxRatesConfiguration configuration;

    @BeforeAll
    public static void init() {
        AnnualTaxRatesValidator annualTaxRatesValidator = new AnnualTaxRatesValidator();

        configuration = new AnnualTaxRatesConfiguration();
        ReflectionTestUtils.setField(configuration, "annualTaxRatesValidator", annualTaxRatesValidator);
    }

    /**
     * Test loading annual tax rates from default configuration file
     */
    @Test
    public void test_LoadAnnualTaxRates() {
        try {

            // This should load the default annual tax rates from the configuration file available in classpath
            configuration.loadAnnualTaxRates();
        }
        catch (Exception e) {
            fail("Error occurred while loading annual tax rates", e);
        }
    }

    /**
     * Test handling file not found when loading annual tax rates from custom configuration file from invalid path
     */
    @Test
    public void test_CustomConfiguration_FileNotFound() {
        try {
            // Pass INVALID file path by setting it to system property
            System.setProperty("annual-tax-rate", "INVALID_PATH");

            configuration.loadAnnualTaxRates();

            fail("Test case failed as it should throw PayslipProcessingException when invalid path specified");
        }
        catch (PayslipProcessingException e) {
            assertEquals(e.getCode(), ErrorCodes.ERR_ANNUAL_TAX_RATES_FILE_NOT_FOUND);
        }
        catch (Exception e) {
            fail("Error occurred while testing for file not found exception", e);
        }
    }
}