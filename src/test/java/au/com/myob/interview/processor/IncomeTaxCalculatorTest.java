package au.com.myob.interview.processor;

import au.com.myob.interview.model.AnnualTaxRates;
import au.com.myob.interview.utils.TestUtils;
import org.junit.jupiter.api.BeforeAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Class to test annual income tax calculations based on the default annual tax rate configurations loaded from configuration file
 */
public class IncomeTaxCalculatorTest {

    private static IncomeTaxCalculator calculator;
    private static AnnualTaxRates defaultAnnualTaxRates;

    @BeforeAll
    public static void init() throws Exception {
        defaultAnnualTaxRates = TestUtils.loadDefaultTaxRates();

        calculator = new IncomeTaxCalculator();
    }

    @ParameterizedTest
    @CsvSource({
        "60000,6000",   // = ((20,000 * 0) + ((40,000 - 20,000) * 0.1) + ((60,000 - 40,000) * 0.2))
        "80001,10000",   // = ((20,000 * 0) + ((40,000 - 20,000) * 0.1) + ((80,000 - 40,000) * 0.2)) + ((80,000 - 80,000) * 0.3)) = 0 + 2000 + 8000 + 0.3
        "96000, 14800", // = ((20,000 * 0) + ((40,000 - 20,000) * 0.1) + ((80,000 - 40,000) * 0.2)) + ((96,000 - 80,000) * 0.3))
        "194000, 45600"  // = ((20,000 * 0) + ((40,000 - 20,000) * 0.1) + ((80,000 - 40,000) * 0.2)) + ((180,000 - 80,000) * 0.3)) + ((194,000 - 180,000) * 0.4))
    })
    void test_CalculateAnnualIncomeTax_ForProvidedInputs(double annualIncome, double expected) {

        // Annual Income tax should match with the expected result for the default annual tax rates that are loaded from default configuration file
        double annualIncomeTax = calculator.calculateAnnualIncomeTax(annualIncome, defaultAnnualTaxRates);
        assertEquals(expected, Math.floor(annualIncomeTax));
    }
}
