package au.com.myob.interview.exception;

public class ErrorCodes {

    private ErrorCodes(){}

    public static final int ERR_RUNTIME = 1111;

    public static final int ERR_INPUT_VALIDATION = 1000;

    public static final int ERR_ANNUAL_TAX_RATES_START_WITH_ZERO = 1005;
    public static final int ERR_DUPLICATES_IN_ANNUAL_TAX_RATES = 1006;
    public static final int ERR_INVALID_RANGE_IN_ANNUAL_TAX_RATES = 1007;

    public static final int ERR_ANNUAL_TAX_RATES_FILE_NOT_FOUND = 1010;

}
