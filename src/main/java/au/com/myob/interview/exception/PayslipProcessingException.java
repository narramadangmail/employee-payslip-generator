package au.com.myob.interview.exception;

/**
 * Custom exception class for payslip processor
 */
public class PayslipProcessingException extends Exception {

    private int code;

    public PayslipProcessingException(int code, String message) {
        super(message);
        this.code = code;
    }

    public PayslipProcessingException(int code, String message, Throwable throwable) {
        super(message, throwable);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
