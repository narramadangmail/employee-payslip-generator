package au.com.myob.interview;

import au.com.myob.interview.exception.ErrorCodes;
import au.com.myob.interview.exception.PayslipProcessingException;
import au.com.myob.interview.processor.EmployeePayslipProcessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Component
public class App {

    private static Logger log = LoggerFactory.getLogger(App.class.getName());

    @Autowired
    private EmployeePayslipProcessor employeePayslipProcessor;

    public static void main(String[] args) {
        try {
            // Initialize application context by annotation configuration
            AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
            ctx.scan("au.com.myob");
            ctx.refresh();

            App app = ctx.getBean(App.class);
            app.processEmployeePayslipGeneration(args);

            ctx.close();
        }
        catch (PayslipProcessingException e) {
            log.error(String.format("Error occurred while processing employee payslip generation : Code - %s  Message - %s", e.getCode(), e.getMessage()), e);
        }
        catch (Exception e) {
            log.error("Runtime error occurred while processing payslip", e);
        }
    }

    public void processEmployeePayslipGeneration(String[] args) throws PayslipProcessingException {
        // Validate user input
        validateUserInput(args);

        String employeeName = args[0];
        double annualIncome = Double.parseDouble(args[1]);

        employeePayslipProcessor.processEmployeePayslipGeneration(employeeName, annualIncome);
    }

    private void validateUserInput(String[] args) throws PayslipProcessingException {

        log.debug("==> User Input :: "+ Arrays.stream(args).collect(Collectors.joining(",")));

        // Validate if two arguments are passed
        if(args == null || args.length != 2) {
            throw new PayslipProcessingException(ErrorCodes.ERR_INPUT_VALIDATION,
                "Invalid arguments passed. Please provide Employee Name and Annual Income as input arguments");
        }

        // Validate if name is not empty
        if(StringUtils.isEmpty(args[0])) {
            throw new PayslipProcessingException(ErrorCodes.ERR_INPUT_VALIDATION,
                "Employee Name provided is empty !!!");
        }

        // Validate if annualIncome is valid number
        if(!NumberUtils.isCreatable(args[1])) {
            throw new PayslipProcessingException(ErrorCodes.ERR_INPUT_VALIDATION,
                "Annual Income provided is not a valid number !!!");
        }
    }
}
