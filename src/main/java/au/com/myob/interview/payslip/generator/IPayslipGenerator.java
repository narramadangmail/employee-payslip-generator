package au.com.myob.interview.payslip.generator;

import au.com.myob.interview.exception.PayslipProcessingException;
import au.com.myob.interview.model.Payslip;

/**
 * Interface to generate employee payslip using the details passed in payslip object
 */
public interface IPayslipGenerator {

    public void generatePayslip(Payslip payslip) throws PayslipProcessingException;
}
