package au.com.myob.interview.payslip.generator;

import au.com.myob.interview.exception.PayslipProcessingException;
import au.com.myob.interview.model.Payslip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Payslip generator using text template engine and printing the payslip to console
 */
@Component
public class ConsolePayslipGenerator implements IPayslipGenerator{

    @Autowired
    @Qualifier("rawTemplateEngine")
    private ITemplateEngine templateEngine;

    @Override
    public void generatePayslip(Payslip payslip) throws PayslipProcessingException {

        Context context = new Context();
        context.setVariable("payslip", payslip);

        String payslipContent = templateEngine.process("raw-template", context);
        System.out.println(payslipContent);
    }
}
