package au.com.myob.interview.configuration;

import au.com.myob.interview.exception.ErrorCodes;
import au.com.myob.interview.exception.PayslipProcessingException;
import au.com.myob.interview.model.AnnualTaxRates;
import au.com.myob.interview.validation.AnnualTaxRatesValidator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Class to load annual tax rates defined in YAML configuration file.
 * <br> Class can load from the path provided in 'annual-tax-rate' system property or from default configuration file available in classpath
 */
@Configuration
public class AnnualTaxRatesConfiguration {

    @Autowired
    AnnualTaxRatesValidator annualTaxRatesValidator;

    @Bean
    public AnnualTaxRates loadAnnualTaxRates() throws PayslipProcessingException {

        AnnualTaxRates annualTaxRates = null;
        InputStream inputStream = null;
        try {
            // Verify if external annual tax rates configuration file is provided.
            // If yes, load it else use the default one available in classpath
            if(StringUtils.isNotEmpty(System.getProperty("annual-tax-rate"))) {
                File file = new File(System.getProperty("annual-tax-rate"));
                inputStream = new FileInputStream(file);
            }
            else {
                inputStream = getClass().getClassLoader().getResourceAsStream("annual-tax-rates.yaml");
            }

            // Parse yaml to AnnualTaxRates instance
            annualTaxRates = new Yaml().loadAs(inputStream, AnnualTaxRates.class);

            // Validate annualTaxRates loaded from configuration file
            annualTaxRatesValidator.validateTaxRatesConfiguration(annualTaxRates);
        }
        catch(FileNotFoundException e) {
            throw new PayslipProcessingException(
                ErrorCodes.ERR_ANNUAL_TAX_RATES_FILE_NOT_FOUND,
                String.format("Error occurred while loading external annual tax rates file as provided - %s", System.getProperty("annual-tax-rate")),
                e);
        }
        catch(Exception e) {
            throw new PayslipProcessingException(
                ErrorCodes.ERR_RUNTIME,
                "Error occurred while loading tax rates",
                e);
        }
        finally {
            if(inputStream != null) try {inputStream.close();}catch (Exception e) { }
        }
        return annualTaxRates;
    }
}