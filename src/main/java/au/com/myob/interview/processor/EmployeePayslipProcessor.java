package au.com.myob.interview.processor;

import au.com.myob.interview.exception.PayslipProcessingException;
import au.com.myob.interview.model.AnnualTaxRates;
import au.com.myob.interview.model.IPayslip;
import au.com.myob.interview.model.Payslip;
import au.com.myob.interview.payslip.generator.IPayslipGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeePayslipProcessor {

    private Logger log = LoggerFactory.getLogger(getClass().getName());

    @Autowired
    private IPayslipGenerator payslipGenerator;

    @Autowired
    private IncomeTaxCalculator incomeTaxCalculator;

    @Autowired
    private AnnualTaxRates annualTaxRates;

    public void processEmployeePayslipGeneration(String employeeName, double annualIncome) throws PayslipProcessingException {

        // Calculate annual income tax
        double annualIncomeTax = incomeTaxCalculator.calculateAnnualIncomeTax(annualIncome, annualTaxRates);

        // Generate Payslip
        payslipGenerator.generatePayslip(
            payslipLambda.createPayslip(
                employeeName,
                annualIncome,
                annualIncomeTax
            )
        );
    }

    // Lambda for initializing payslip model object based on the provided annualIncome and annualIncomeTax
    private IPayslip payslipLambda = (employeeName, annualIncome, annualIncomeTax) -> {

        // Prepare monthly amounts
        double grossPerMonth = Math.floor(annualIncome/12);
        double taxPerMonth = Math.floor(annualIncomeTax/12);
        double netPerMonth = grossPerMonth - taxPerMonth;

        // create instance of payslip with all the mandatory params
        Payslip payslip
            = new Payslip(
                employeeName,
                grossPerMonth,
                taxPerMonth,
                netPerMonth);

        return payslip;
    };
}
