package au.com.myob.interview.processor;

import au.com.myob.interview.model.AnnualTaxRates;
import au.com.myob.interview.model.TaxRate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to calculate annual income tax based on the loaded tax rates
 */
@Component
public class IncomeTaxCalculator {

    private Logger log = LoggerFactory.getLogger(getClass().getName());

    /**
     * Method to calculate annual income tax based on the provided annualSalary and tax rates
     * @param annualSalary
     * @param annualTaxRates
     * @return annualIncomeTax
     */
    public double calculateAnnualIncomeTax(double annualSalary, AnnualTaxRates annualTaxRates) {

        // Assign annualSalary to salary which will be used as part of calculating tax as per tax rates
        double salary = annualSalary;
        double annualIncomeTax = 0;

        // Sort taxRates by tax band before looping over to calculate annual tax
        List<TaxRate> taxRates = annualTaxRates.getTaxRates().stream().sorted(Comparator.comparingInt(TaxRate::getBand)).collect(Collectors.toList());

        // Loop over taxRates to calculate annual tax
        for(TaxRate taxRate : taxRates) {

            if(annualSalary >= taxRate.getLowerCutOff()) {

                // Prepare the taxableIncome as per the defined tax rates
                double taxableIncome = annualSalary <= taxRate.getUpperCutOff() && annualSalary >= taxRate.getLowerCutOff()
                    ? salary
                    : taxRate.getUpperCutOff() - taxRate.getLowerCutOff();

                // Calculate tax as per the rate and add to annualTax
                annualIncomeTax += taxableIncome * taxRate.getTaxPerDollar();

                // subtract the taxableIncome from the overall salary for every loop
                salary -= taxableIncome;

                log.debug(String.format("===> slice - %s  Annual Tax - %s   Salary - %s ", taxableIncome, annualIncomeTax, salary));
            }
        }
        return annualIncomeTax;
    }
}
