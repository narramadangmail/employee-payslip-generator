package au.com.myob.interview.validation;

import au.com.myob.interview.exception.ErrorCodes;
import au.com.myob.interview.exception.PayslipProcessingException;
import au.com.myob.interview.model.AnnualTaxRates;
import au.com.myob.interview.model.TaxRate;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Class to validate if annual tax rates are defined properly before processing employee payslip.
 */
@Component
public class AnnualTaxRatesValidator {

    /**
     * Validate annual tax rates loaded from the configuration
     *
     * 1. Validate if tax rates start with 0 taxable income in the range defined
     * 2. Validate for duplicate bands defined for tax rate
     * 3. Validate for duplicate upper or lower cutoff amounts defined in the tax rate ranges
     * 4. Validate for proper ranges defined between bands and also valid range set for lower and upper cutoff limit
     * @param annualTaxRates
     * @throws PayslipProcessingException
     */
    public void validateTaxRatesConfiguration(AnnualTaxRates annualTaxRates) throws PayslipProcessingException {

        // Validate lowerCutoff is always 0 for the first tax rate band
        if(annualTaxRates.getTaxRates().get(0).getLowerCutOff() != 0) {
            throw new PayslipProcessingException(
                ErrorCodes.ERR_ANNUAL_TAX_RATES_START_WITH_ZERO,
                "Tax Rates range should start with 0 for lower cut off");
        }

        // Validate if band is unique
        throwIfDuplicatesExists(annualTaxRates, TaxRate::getBand, "Band");

        // Validate if band is unique
        throwIfDuplicatesExists(annualTaxRates, TaxRate::getLowerCutOff, "Lower Cut off");

        // Validate if band is unique
        throwIfDuplicatesExists(annualTaxRates, TaxRate::getUpperCutOff, "Upper Cut off");

        // Validate taxRates range configured
        validateTaxRateRanges(annualTaxRates);
    }

    /**
     * Method to validate if the provided field holds any duplicates in list of tax rates loaded from configuration
     * @param annualTaxRates
     * @param keyExtractor
     * @param field
     * @throws PayslipProcessingException
     */
    private void throwIfDuplicatesExists(AnnualTaxRates annualTaxRates, Function<TaxRate, ?> keyExtractor, String field) throws PayslipProcessingException {

        // If duplicates available, then field value count will not be equal to the size of tax rates loaded from configuration
        if(annualTaxRates.getTaxRates().stream().filter(distinctByKey(keyExtractor)).count() != annualTaxRates.getTaxRates().size()) {
            throw new PayslipProcessingException(
                ErrorCodes.ERR_DUPLICATES_IN_ANNUAL_TAX_RATES,
                String.format("Duplicate values found for field - %s", field)
            );
        }
    }

    /**
     * Validate ranges defined for different tax rates
     * @param annualTaxRates
     * @throws PayslipProcessingException
     */
    private void validateTaxRateRanges(AnnualTaxRates annualTaxRates) throws PayslipProcessingException {

        // Set local variable to assign the upper cutoff limit in the current iteration for validating range in next iteration
        double taxableIncome = 0;

        // Sort taxRates by tax band before looping over to calculate annual tax
        List<TaxRate> taxRates = annualTaxRates.getTaxRates().stream().sorted(Comparator.comparingInt(TaxRate::getBand)).collect(Collectors.toList());
        for (TaxRate taxRate : taxRates) {

            // Validate if tax rate ranges does not continue in sequence
            if(taxRate.getLowerCutOff() != 0
                && !(taxableIncome+1 == taxRate.getLowerCutOff())) {
                throw new PayslipProcessingException(
                    ErrorCodes.ERR_INVALID_RANGE_IN_ANNUAL_TAX_RATES,
                    "Invalid range defined in tax rates configuration.");
            }

            // Validate if upper cut off limit is lesser than lower cut off limit
            if(taxRate.getLowerCutOff() > taxRate.getUpperCutOff()) {
                throw new PayslipProcessingException(
                    ErrorCodes.ERR_INVALID_RANGE_IN_ANNUAL_TAX_RATES,
                    String.format("Lower cutoff %s configured is greater than upper cutoff %s.", taxRate.getLowerCutOff(), taxRate.getUpperCutOff()));
            }

            taxableIncome = taxRate.getUpperCutOff();
        }
    }

    // Verify if class variable holds distinct value in the stream
    private  <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
