package au.com.myob.interview.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class TaxRate implements Serializable {

    private int band;
    private int lowerCutOff;
    private int upperCutOff;
    private float taxPerDollar;

    public void setUpperCutOff(int upperCutOff) {
        if(upperCutOff == -1)
            upperCutOff = Integer.MAX_VALUE;

        this.upperCutOff = upperCutOff;
    }
}
