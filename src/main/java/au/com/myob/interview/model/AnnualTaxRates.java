package au.com.myob.interview.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AnnualTaxRates implements Serializable {

    private List<TaxRate> taxRates;
}
