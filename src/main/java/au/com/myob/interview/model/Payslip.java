package au.com.myob.interview.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Payslip {

    private String employeeName;
    private Double grossMonthlyIncome;
    private Double monthlyIncomeTax;
    private Double netMonthlyIncome;
}
