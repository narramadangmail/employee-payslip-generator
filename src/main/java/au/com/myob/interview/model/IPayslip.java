package au.com.myob.interview.model;

/**
 * Lambda for initializing payslip model object based on the provided annualIncome and annualIncomeTax
 */
@FunctionalInterface
public interface IPayslip {

    /**
     * Create instance of payslip based on the provided input arguments
     *
     * @param employeeName
     * @param annualIncome
     * @param annualIncomeTax
     * @return payslip
     */
    Payslip createPayslip(String employeeName, double annualIncome, double annualIncomeTax);
}
