# Employee Monthly Payslip Generation

## Introduction

This is a console based application which generates payslip for the provided employee annual salary.

Payslip includes monthly tax that is derived from the annual salary and the current applicable tax rates.

### Annual Tax Rates
As part of this application, below is the tax rate that will be configured within the application. But this configuration can be overridden by customized tax rates configuration file provided during execution.

|Taxable income|Tax on this income|
| ---------- | ----------|
|$0 - $20,000|$0|
|$20,001 - $40,000|10c for each $1 over $20,000|
|$40,001 - $80,000|20c for each $1 over $40,000|
|$80,001 - $180,000|30c for each $1 over $80,000|
|$180,001 and over|40c for each $1 over $180,000| 

### Considerations / Assumptions

Below are few considerations or assumptions made when implementing this application:

* Amount fields are considered as Double data types and thus output includes decimal values.
* Annual Tax Rates are configured in YAML file
* Provide option to pass customized tax rates configuration file during execution.

### Expected Sample Output

Payslip generated and printed in the console when executed the application should match to what is provided below.

```
λ java -jar target\employee-payslip-generator-jar-with-dependencies.jar "Mary Song" 60000

Monthly Payslip for: "Mary Song"
Gross Monthly Income: $5000.0
Monthly Income Tax: $500.0
Net Monthly Income: $4500.0
``` 

## Technologies

Application is built in Java with the following framework and libraries which are managed by Maven

* Java 8
* Spring Framework 5.2.4
* Project Lombok 1.18.12
* Thymeleaf 3.0.11
* JUnit 5
* Mockito 3
* JaCoCo 0.8.5
* And few other util libraries...

## Setting up the project and Executing the Application

Ensure the latest release of `Maven 3` in configured properly on the machine on which this project will be setup and executed.

Below are series of steps that are needed to execute the application:

* Extract the uploaded archive file or clone the [git repository](https://bitbucket.org/narramadangmail/employee-payslip-generator).
* Run command `mvn clean package` from root of the project to generate executable jar file
* Run the below command from root of the project to execute the application with employee name and his/her salary provided

```
λ java -jar target\employee-payslip-generator-jar-with-dependencies.jar "Mary Song" 60000
```

* Compare payslip output generated to the expected sample output defined above
* Execute the application with additional tests to compare the output to expected results. For ex:

```
λ java -jar target\employee-payslip-generator-jar-with-dependencies.jar "Mary Song" 96000
Monthly Payslip for: "Mary Song"
Gross Monthly Income: $8000.0
Monthly Income Tax: $1233.0
Net Monthly Income: $6767.0

λ java -jar target\employee-payslip-generator-jar-with-dependencies.jar "Mary Song" 194000
Monthly Payslip for: "Mary Song"
Gross Monthly Income: $16166.0
Monthly Income Tax: $3800.0
Net Monthly Income: $12366.0
```

* To execute the application with tax rates other than what is configured within the application, take copy of `src\main\resources\annual-tax-rates.yaml` and update to the new tax rates and run the below command

```
λ java -jar -Dannual-tax-rate=<PATH_TO_NEW_TAXRATES_CONFIG_YAML_FILE> target\employee-payslip-generator-jar-with-dependencies.jar "Mary Song" 60000
```

## Code Coverage Results

JaCoCo is configured with maven to capture the code covered as part of the unit tests that are executed when packaging the application.

To generate the report, run `mvn verify` from the root of the project. This will generate the coverage results in `target\site\jacoco` folder. Open `index.html` to view the results. It should see something as below:

![Code Coverage Result](codecoverage-result.png)